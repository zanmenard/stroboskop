# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://zanmenard@bitbucket.org/zanmenard/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/zanmenard/stroboskop/commits/09bc069d910b575f243123e8ba72550dd5be5f4a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/zanmenard/stroboskop/commits/09430598bf2b4ce343cca720ebad08583333df6a

Naloga 6.3.2:
https://bitbucket.org/zanmenard/stroboskop/commits/3fcc8baa169a1269303af6cbbf8a9560cab7cb0e

Naloga 6.3.3:
https://bitbucket.org/zanmenard/stroboskop/commits/1baea9dd17133580092d5aac7cfa650aead9466a

Naloga 6.3.4:
https://bitbucket.org/zanmenard/stroboskop/commits/e0863a2ed960d466d0fa18cd1e5af4ebc255e15d

Naloga 6.3.5:

git checkout master
git merge izgled
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/zanmenard/stroboskop/commits/b5ece22625622e053bf6dfe3ec4b616488625e23

Naloga 6.4.2:
https://bitbucket.org/zanmenard/stroboskop/commits/cb8264505a45c2114672e6f25d5b52ea60a43fe1

Naloga 6.4.3:
https://bitbucket.org/zanmenard/stroboskop/commits/6a4b38a92ea709d328670b348ae21d2e9d5207ae

Naloga 6.4.4:
https://bitbucket.org/zanmenard/stroboskop/commits/893b8ca4ff833494f62e36480b289621d53b7556